/*
Navicat PGSQL Data Transfer

Source Server         : postgres
Source Server Version : 90601
Source Host           : localhost:5432
Source Database       : latihan
Source Schema         : dbo

Target Server Type    : PGSQL
Target Server Version : 90601
File Encoding         : 65001

Date: 2017-03-21 13:19:45
*/


-- ----------------------------
-- Sequence structure for Emp_tbl_id
-- ----------------------------
DROP SEQUENCE IF EXISTS "Emp_tbl_id";
CREATE SEQUENCE "Emp_tbl_id"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;
SELECT setval('"dbo"."Emp_tbl_id"', 3, true);

-- ----------------------------
-- Sequence structure for Movies_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "Movies_id_seq";
CREATE SEQUENCE "Movies_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 7
 CACHE 1;
SELECT setval('"dbo"."Movies_id_seq"', 7, true);

-- ----------------------------
-- Sequence structure for squence_postgres
-- ----------------------------
DROP SEQUENCE IF EXISTS "squence_postgres";
CREATE SEQUENCE "squence_postgres"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;
SELECT setval('"dbo"."squence_postgres"', 1, true);

-- ----------------------------
-- Table structure for emp_tbl
-- ----------------------------
DROP TABLE IF EXISTS "emp_tbl";
CREATE TABLE "emp_tbl" (
"id" int4 DEFAULT nextval('"dbo"."Emp_tbl_id"'::regclass) NOT NULL,
"name" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"address" varchar(255) COLLATE "default",
"telephone" varchar(255) COLLATE "default" DEFAULT nextval('"dbo".squence_postgres'::regclass)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of emp_tbl
-- ----------------------------
BEGIN;
INSERT INTO "emp_tbl" VALUES ('2', 'Jono', 'jono@gmailcom', 'Tes', '001');
INSERT INTO "emp_tbl" VALUES ('6', 'Asrori', 'rorijoe@gmail.com', 'bandung', '085801766481');
COMMIT;

-- ----------------------------
-- Table structure for Movies
-- ----------------------------
DROP TABLE IF EXISTS "Movies";
CREATE TABLE "Movies" (
"ID" int4 DEFAULT nextval('"dbo"."Movies_id_seq"'::regclass) NOT NULL,
"Title" varchar(255) COLLATE "default",
"ReleaseDate" date,
"Genre" varchar(255) COLLATE "default",
"Price" numeric(10,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of Movies
-- ----------------------------
BEGIN;
INSERT INTO "Movies" VALUES ('1', 'Tes', '2017-01-12', 'Tes', '1900.00');
INSERT INTO "Movies" VALUES ('3', 'Tes 2', '2017-01-13', 'Tes Lagi', '2000.00');
INSERT INTO "Movies" VALUES ('4', 'd', '2017-03-21', 'd', '1.00');
COMMIT;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Uniques structure for table emp_tbl
-- ----------------------------
ALTER TABLE "emp_tbl" ADD UNIQUE ("id");

-- ----------------------------
-- Primary Key structure for table emp_tbl
-- ----------------------------
ALTER TABLE "emp_tbl" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table Movies
-- ----------------------------
ALTER TABLE "Movies" ADD UNIQUE ("ID");

-- ----------------------------
-- Primary Key structure for table Movies
-- ----------------------------
ALTER TABLE "Movies" ADD PRIMARY KEY ("ID");
