<%-- 
    Document   : EmployeeForm
    Created on : Mar 14, 2017, 4:53:30 PM
    Author     : ASUS
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New/Edit Karyawan</title>
        <spring:url value="resources/core/css/bootstrap.min.css" var="bootstrapCss" />
        <link href="${bootstrapCss}" rel="stylesheet" />
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header"><a class="navbar-brand" href="#">Beranda</a></div>
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4>New/Edit Karyawan</h4>
                    <form:form action="saveEmployee" method="post" modelAttribute="employee">
                        <table class="table">
                            <form:hidden path="id"/>
                            <tr>
                                <td>Nama:</td>
                                <td><form:input placeholder="Masukan Nama" path="name" type="text" class="form-control" required="required" /></td>
                            </tr>
                            <tr>
                                <td>Email:</td>
                                <td><form:input placeholder="Masukan Email" path="email" type="email" class="form-control" required="required" /></td>
                            </tr>
                            <tr>
                                <td>Alamat:</td>
                                <td><form:input placeholder="Masukan Alamat" path="address" type="text" class="form-control" required="required" /></td>
                            </tr>
                            <tr>
                                <td>Telephone:</td>
                                <td><form:input placeholder="Masukan Telephon" path="telephone" type="text" class="form-control" required="required" /></td>
                            </tr>
                            <tr>
                                <td><input type="submit" value="Save" class="btn btn-sm btn-primary"></td>
                                <td><a href="/DemoSpring" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-backward"></span>&nbsp;Kembali</a></td>
                            </tr>
                        </table>
                    </form:form>
                </div>
            </div>
        </div>
    
        <hr/>
        <footer><p class="text-center">&copy; 2017-2018</p></footer>
        <spring:url value="/resources/core/js/hello.js" var="coreJs" />
        <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
        
        <script src="${coreJs}"></script>
        <script src="${bootstrapJs}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </body>
</html>