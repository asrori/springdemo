<%-- 
    Document   : home
    Created on : Mar 14, 2017, 4:53:30 PM
    Author     : ASUS
--%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Management Karyawan</title>
        <spring:url value="resources/core/css/bootstrap.min.css" var="bootstrapCss" />
        <link href="${bootstrapCss}" rel="stylesheet" />
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header"><a class="navbar-brand" href="#">Beranda</a></div>
            </div>
        </nav>
    
	<div class="container">
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <caption class="text-center text-capitalize"><strong>Tabel Data</strong></caption>
                        <thead>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Alamat</th>
                            <th>Telephone</th>
                            <th>Action</th>
                        </thead>
                           
                        <tbody>
                        <c:set var="s" value="${0}" scope="page" />
                        <c:forEach var="employee" items="${listEmployee}">
                            <tr>
                                <c:set var="s" value="${s+1}" scope="page" />
                                <td><c:out value="${s}" /></td>
                                <td>${employee.name}</td>
                                <td>${employee.email}</td>
                                <td>${employee.address}</td>
                                <td>${employee.telephone}</td>
                                <td><a href="editEmployee?id=${employee.id}" class="btn btn-sm btn-info"><span class="glyphicon glyphicon-edit"></span>&nbsp;Edit</a>&nbsp;&nbsp;<a href="deleteEmployee?id=${employee.id}" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <a href="newEmployee" class="btn btn-sm btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</a>
                </div>
            </div>
	</div>
        
        <hr/>
        <footer><p class="text-center">&copy; 2017-2018</p></footer>
        <spring:url value="/resources/core/js/hello.js" var="coreJs" />
        <spring:url value="/resources/core/js/bootstrap.min.js" var="bootstrapJs" />
        
        <script src="${coreJs}"></script>
        <script src="${bootstrapJs}"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </body>
</html>